<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Hello World - JSP tutorial</title>
		<link href="favicon.ico?" type="image/x-icon" />
		
		<!--Stylesheet-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
        <link rel="stylesheet" href="./reactjs/public/style.css">
	</head>
	<body>
		<h1><%= "Hello World!" %></h1>

		<form action="helloWorldServlet" method="post">
			Enter your name: <input type="text" name="yourName" size="20">
			<input type="submit" value="Call Servlet" />
		</form>

		<!-- <div id="react-app"></div> -->
		<div class="container">
          <div class="col-md-4 col-md-offset-4">
            <div id="container"></div>
          </div>
        </div>

		<script type="text/javascript" src="./reactjs/public/bundle.js"></script>

	</body>
</html>
